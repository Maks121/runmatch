using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBoard : MonoBehaviour
{
    public List<GameObject> ListTile;
    // Start is called before the first frame update

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Tile"))
        {
            
            ListTile.Add(collision.gameObject);
            TileControl del = collision.gameObject.GetComponent<TileControl>();
            del.activeTile = true;
            if (del.levelDefence <= 2)
            {
                del.tileDelegate += OnTileDestroy;
            }
            
        }
    }
    void OnTileDestroy(GameObject enemy)
    {// �� ������� ����� �� enemiesInRange
        ListTile.Remove(enemy);
    }
}
