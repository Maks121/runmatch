using System;
using UnityEngine;

public class EventStopGame : MonoBehaviour
{
    public delegate void eventDelegate();
    public  event eventDelegate StopGame;
    public  event eventDelegate StopGameOnStart;
    public  event eventDelegate RunGame;
    public  event eventDelegate RunGameOnStart;

    public static EventStopGame Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    public void Active()
    {
        StopGame?.Invoke();
    }
    public void EnActive()
    {
        RunGame?.Invoke();
    }
    public void ActiveOnStart()
    {
        StopGameOnStart?.Invoke();
    }
    public void EnActiveOnStart()
    {
        RunGameOnStart?.Invoke();
    }
}
