using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ActivationBonuses : MonoBehaviour
{

    public bool activeLightning;
    public bool activeDynamite;
    public bool activeTimer;
    [SerializeField] private Bag bag;
    [SerializeField] private StartBoard startBoard;
    [SerializeField] private RunBoard runBoard;

    public GameObject buttonTimer;
    public Sprite spriteExplosive;

    public GameObject prefabLightning;
    public GameObject panelParent;
    

    

    public void ActiveBonus(GameObject obj)
    {
        if (activeDynamite) ActiveDynamite(obj);
        else if (activeLightning) ActiveLightning(obj);
        

    }
    private void ActiveDynamite(GameObject obj)
    {
        var sprite = obj.GetComponent<SpriteRenderer>().sprite;
        obj.GetComponent<SpriteRenderer>().sprite = spriteExplosive;
        Destroy(obj, 0.2f);
        bag.BonusDynamite--;
        activeDynamite = false;

        Collider2D[] box = Physics2D.OverlapBoxAll(obj.transform.position, Vector2.one, 0f);
        foreach (var a in box)
        {
            if (a.CompareTag("Tile"))
            {
                var takeTile = a.GetComponent<TileControl>();
                if (takeTile.levelDefence == 2)
                {
                    takeTile.ActionTileWithObj(a.gameObject);
                }
            }  
        }
    }
    private void ActiveLightning(GameObject obj)
    {
        var sprite = obj.GetComponent<SpriteRenderer>().sprite;
        
        foreach (var item in startBoard.ListTile)
        {
            if (sprite == item.GetComponent<SpriteRenderer>().sprite)
            {
                item.GetComponent<TileControl>().ActionTileWithObj(item.gameObject);
                Vector3 vector = new Vector3(item.transform.position.x, item.transform.position.y + 1, item.transform.position.z);
                GameObject Ob = Instantiate(prefabLightning,vector,Quaternion.identity);
                Destroy(Ob, .5f);
            }
        }
        bag.BonusLightning--;
        activeLightning = false;
    }
    public void ActiveTimer()
    {
        runBoard.speed = .1f;
        StartCoroutine(timerActive(5));
        bag.BonusTimer--;
    }
    private IEnumerator timerActive(int time)
    {
        buttonTimer.GetComponent<Button>().interactable = false;
        int a = time;
        while (a > 0)
        {

            yield return new WaitForSeconds(1f);
            a--;
            if (a == 0)
            {
                runBoard.speed = 0.21f;
                buttonTimer.GetComponent<Button>().interactable = true;
            }
        }
        
    } 

}
