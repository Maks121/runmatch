using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Bag : MonoBehaviour
{
    [SerializeField] private Save save;
    public Text textGold;
    public Text textLightning;
    public Text textDynamite;
    public Text textTimer;
   

    public Text textLightningInGame;
    public Text textDynamiteInGame;
    public Text textTimerInGame;

    public GameObject[] Bonus;

    private int gold;
    private int bonusLightning;
    private int bonusDynamite;
    private int bonusTimer;

    

    private void Start()
    {
        save.LG();
        Gold = DataBase.Instance.Gold;
        BonusLightning = DataBase.Instance.BonusLightning;
        BonusDynamite = DataBase.Instance.BonusDynamite;
        BonusTimer = DataBase.Instance.BonusTimer;
        
    }

    public int Gold
    {
        get
        {
            return DataBase.Instance.Gold;
        }
        set
        {
            DataBase.Instance.Gold = value;
            textGold.text = DataBase.Instance.Gold.ToString();
            
            Bonus[0].GetComponent<Animation>().Play(); 
        }
    }
    public int BonusLightning
    {
        get
        {
            return DataBase.Instance.BonusLightning;
        }
        set
        {
            DataBase.Instance.BonusLightning = value;
            textLightning.text = DataBase.Instance.BonusLightning.ToString();
            textLightningInGame.text = DataBase.Instance.BonusLightning.ToString();
            Bonus[1].GetComponent<Animation>().Play();
        }
    }
    public int BonusDynamite
    {
        get{ return DataBase.Instance.BonusDynamite; }
        set
        {
            DataBase.Instance.BonusDynamite = value;
            textDynamite.text = DataBase.Instance.BonusDynamite.ToString();
            textDynamiteInGame.text = DataBase.Instance.BonusDynamite.ToString();
            Bonus[2].GetComponent<Animation>().Play();
            
        }
    }
    public int BonusTimer
    {
        get
        {
            return DataBase.Instance.BonusTimer;
        }
        set
        {
            DataBase.Instance.BonusTimer = value;
            textTimer.text = DataBase.Instance.BonusTimer.ToString();
            textTimerInGame.text = DataBase.Instance.BonusTimer.ToString();
            Bonus[3].GetComponent<Animation>().Play();
            
        }

    }
    public void SaveInDataBase()
    {

        save.SG();
    }

}
