using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Save : MonoBehaviour
{
    public void SG()
    {
        SaveGame();
    }
    public void LG()
    {
        LoadGame();
    }
    
    private void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SaveDateRunMatch.dat");
        SaveDate data = new SaveDate();
        data.money = DataBase.Instance.Gold;
        data.score = DataBase.Instance.Score;
        data.countLevel = DataBase.Instance.CountLevel;
        data.lvlStar = DataBase.Instance.LvlStar;
        data.soundOn = DataBase.Instance.SoundOn;
        data.currentLanguage = DataBase.Instance.CurrentLanguage;
        data.bonusDynamite = DataBase.Instance.BonusDynamite;
        data.bonusLightning = DataBase.Instance.BonusLightning;
        data.bonusTimer = DataBase.Instance.BonusTimer;
       
        bf.Serialize(file, data);
        file.Close();
    }
    
    private void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveDateRunMatch.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/SaveDateRunMatch.dat", FileMode.Open);
            SaveDate data = (SaveDate)bf.Deserialize(file);
            file.Close();
            DataBase.Instance.Gold = data.money;
            DataBase.Instance.Score = data.score;
            DataBase.Instance.CountLevel = data.countLevel;
            DataBase.Instance.LvlStar = data.lvlStar;
            DataBase.Instance.SoundOn =  data.soundOn;
            DataBase.Instance.CurrentLanguage = data.currentLanguage;
            DataBase.Instance.BonusDynamite = data.bonusDynamite;
            DataBase.Instance.BonusLightning = data.bonusLightning;
            DataBase.Instance.BonusTimer = data.bonusTimer;

        }
        else Debug.Log("There is no save data!!");


    }
}
[Serializable]
class SaveDate
{
    public int money;
    public int score;
    public int countLevel;
    public int bonusLightning;
    public int bonusDynamite;
    public int bonusTimer;
    public int[] lvlStar;
    public bool soundOn;
    public string currentLanguage;
}
