using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileControl : MonoBehaviour
{
    private static Color selectedColor = new Color(.5f, .5f, .5f, 1.0f);
    private static TileControl previousSelected = null;

    private SpriteRenderer render;
    private bool isSelected = false;
	public GameObject partical;
	public GameObject particalStone;
	public int levelDefence;
	public delegate void TileDelegate(GameObject tile);
	public TileDelegate tileDelegate;
	public bool activeTile = false; 

	[SerializeField] private GameMode gameMode;
	[SerializeField] private ActivationBonuses bonus;

	public List<GameObject> matchingTiles = new List<GameObject>();
	
	void Awake()
	{
		render = GetComponent<SpriteRenderer>();
	}
    private void Start()
    {
		gameMode = FindObjectOfType<GameMode>();
		bonus = FindObjectOfType<ActivationBonuses>();
	}
	
    private void Select()
	{ // ����� ������� ����, ��������
		isSelected = true;
		render.color = selectedColor;
		previousSelected = gameObject.GetComponent<TileControl>();
	}
    private void OnDestroy()
    {
		if (tileDelegate != null)
		{
			tileDelegate(gameObject);
		}
		Instantiate(partical, gameObject.transform);
	}
    private void Deselect()
	{
		// ����� ����� �� �������
		isSelected = false;
		render.color = Color.white;
		previousSelected = null;
	}
	void OnMouseDown()
	{
        if (activeTile)
        {
			if (bonus.activeDynamite || bonus.activeLightning)
			{
				bonus.ActiveBonus(gameObject);
			}
			else
			{
				if (render.sprite == null)
				{
					return;
				}

				if (isSelected)
				{ // 2 Is it already selected?
					Deselect();
				}
				else
				{
					if (previousSelected == null)
					{
						Select();
					}
					else
					{
						TakeSprite(previousSelected.render, previousSelected.gameObject);
					}
				}
			}
		}
	}
    
    private void TakeSprite(SpriteRenderer render2, GameObject obj)
    {
		
		if (render.sprite == render2.sprite)
		{
			Select();
			ClearMatch(obj);
		}
		else 
		{
			previousSelected.Deselect();
		}
    }
	private void ClearMatch(GameObject obj)
	{
		matchingTiles.Add(obj);
		if (gameObject.GetComponent<SpriteRenderer>().sprite == render.sprite)
		{
			matchingTiles.Add(gameObject);
			if (matchingTiles.Count >= 2)
			{

				ActionTileWithObj(obj);
				ActionTileWithObj(gameObject);
				matchingTiles.Clear();
				previousSelected.Deselect();
			}
		}
	}
	public void ActionTileWithObj(GameObject obj)
    {
		if (obj.GetComponent<TileControl>().levelDefence == 1)
		{
			Sprite sprite = obj.GetComponent<SpriteRenderer>().sprite;
			obj.GetComponent<SpriteRenderer>().sprite = null;
			Instantiate(partical, obj.transform);
			Destroy(obj, .5f);
			gameMode.Task(sprite);
		}
		if (obj.GetComponent<TileControl>().levelDefence == 2)
		{
			Instantiate(particalStone, obj.transform);
			Destroy(obj.transform.GetChild(0).gameObject);
			obj.GetComponent<TileControl>().levelDefence--;
			obj.GetComponent<SpriteRenderer>().color = Color.white;
			obj.GetComponent<TileControl>().isSelected = false;

		}
	}
	
}
