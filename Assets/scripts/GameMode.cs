using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;

public class GameMode : MonoBehaviour
{
    [SerializeField] private LevelManager LM;
    [SerializeField] private Save save;
    [SerializeField] private UIinGame UI;
    [SerializeField] private LevelComplate levelComplate;

    public Text textTimer;
    private bool active = true;
    private bool active1 = true;

    private bool stopGame = false;
    private float stoptime;
    public int[] countItem;

    public Text textCountLevelComlate;

    // Start is called before the first frame update
    void Start()
    {
  
        UI = gameObject.GetComponent<UIinGame>();
        LM = FindObjectOfType<LevelManager>();
        countItem = LM.curLvl.countItem;
        if (LM.curLvl.gameMode == Level.GameMode.Timer)
        {
            stoptime = LM.curLvl.timeLevel;
            EventStopGame.Instance.RunGame += EventStopGame_RunGame;
            EventStopGame.Instance.StopGame += EventStopGame_StopGame;
            TimerActive();
        }
    }

    private void EventStopGame_RunGame()
    {
            stopGame = false;
            StartCoroutine(StartTimer(stoptime));
    }

    private void EventStopGame_StopGame()
    {
            stopGame = true;
    }

    private void Update()
    {
        if (LM.curLvl.gameMode == Level.GameMode.Timer)
        {
            if (UI.TileParent.transform.childCount <= 6 && active1 == true)
            {//������ �������� �� ���������� 
                Complate();
            }
            else if (UI.TileParent.transform.childCount == 0 && active1 == true)
            {
                ComplateNull();
            }
        }
            
    }
    private void TimerActive()
    {
        textTimer.text = LM.curLvl.timeLevel.ToString();
    }

    public IEnumerator<WaitForSeconds> StartTimer(float time)
    {
        //������� ������
        float timer = time;
        while (timer > 0)
        {
            if (stopGame)
            {
                stoptime = timer;
                
                break;
            }
            yield return new WaitForSeconds(1.0f);
            timer--;
            textTimer.text = timer.ToString();
            
        }
        if (timer == 0)
        {
            //��� ������ �� ��������� �������!
            FinishAction();
            Debug.Log("time out");
            EventStopGame.Instance.Active();
            EventStopGame.Instance.ActiveOnStart();
        }
    } 
    private void FinishAction()
    {
        //�������� �� ����� �������
        if (active)
        {
            UI.panelGameOver.SetActive(!UI.panelGameOver.activeSelf);
            active = false;
        }

    }
    public void Task(Sprite sprite)
    {// ����������� ���������� �������

        for (int i = 0; i < LM.curLvl.spriteTask.Length; i++)
        {
            ;
            if (sprite == LM.curLvl.spriteTask[i])
            {
                if (countItem[i] >= 1)
                {
                    countItem[i] -= 1;
                    UI.panel.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = countItem[i].ToString();
                }

                if (countItem.Length >= 0)
                {

                    bool result = countItem.All(x => x == 0);
                    if (LM.curLvl.gameMode == Level.GameMode.Task && result)
                    {
                        StartCoroutine(ComplateAfter());
                    }
                }
            }
        }
    }
    public void Complate()
    {// �������� �� ��������� �� ��� ����� ���� � ������. ���� ��� ���������� � ��������� ����
        int childCount = UI.TileParent.transform.childCount;
        if (childCount != 0)
        {
            bool[] activeLocal = new bool[childCount];
            bool[] activeLocal1 = new bool[childCount];
            for (int i = 0; i < childCount; i++)
            {
                var sprite = UI.TileParent.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite;
                for (int a = 0; a < childCount; a++)
                {
                    if (a == i) { activeLocal1[a] = true; continue; }

                    var sprite1 = UI.TileParent.transform.GetChild(a).GetComponent<SpriteRenderer>().sprite;
                    if (sprite != sprite1)
                    {
                        activeLocal1[a] = true;
                    }
                    else
                    {
                        activeLocal1[a] = false;
                        break;
                    }

                }
                if (activeLocal1.All(x => x))
                {
                    activeLocal[i] = true;
                }

            }
            if (activeLocal.All(x => x))
            {
                active1 = false;
                StartCoroutine(ComplateAfter());
            }
        }
        

    }
    public void ComplateNull()
    {//��������� ���� ������ ������ ����������
        active1 = false;
        StartCoroutine(ComplateAfter());
    }
    private void AnimationAfterComplate()
    {
        //����������� ���� ������, ���� �� �������
        var Tile = UI.TileParent.transform;
        for (int i = 0; i < Tile.childCount; i++)
        {
            GameObject gm = Tile.GetChild(i).GetComponent<TileControl>().partical;
            Transform transform = Tile.GetChild(i);
            Instantiate(gm, transform);
            Tile.GetChild(i).GetComponent<SpriteRenderer>().sprite = null;
            Destroy(Tile.GetChild(i).gameObject, .5f);
        }
    }
    private IEnumerator ComplateAfter()
    {
        // �������� �� ����� ����������� ������
        AnimationAfterComplate();
        EventStopGame.Instance.Active();
        EventStopGame.Instance.ActiveOnStart();
        yield return new WaitForSeconds(1f);
        UI.panelComplate.transform.DOScale(Vector2.one, 0.5f);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(levelComplate.AnimationStar());
        if (DataBase.Instance.LvlStar[LM.currentIndex] < levelComplate.countStar)
        {
            DataBase.Instance.LvlStar[LM.currentIndex] = levelComplate.countStar;
        }
        textCountLevelComlate.text = (LM.currentIndex + 1).ToString();
        save.SG();
    }
}
