using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class DataBase : MonoBehaviour
{
    [SerializeField] private Save save;
    [SerializeField] private int gold;
    [SerializeField] private int score;
    [SerializeField] private int bonusLightning;
    [SerializeField] private int bonusDynamite;
    [SerializeField] private int bonusTimer;


    [SerializeField] private int countLevel;
    [SerializeField] private int[] lvlStar;
    [SerializeField] private bool soundOn;
    [SerializeField] private string currentLanguage;

    public bool SoundOn { get => soundOn; set => soundOn = value; }
    public int[] LvlStar  { get => lvlStar; set => lvlStar = value; }
    public int Score { get => score; set => score = value; }
    public int CountLevel { get => countLevel; set => countLevel = value; }
    public int Gold { get => gold; set => gold = value; }
    public int BonusLightning { get => bonusLightning; set => bonusLightning = value; }
    public int BonusDynamite { get => bonusDynamite; set => bonusDynamite = value; }
    public int BonusTimer { get => bonusTimer; set => bonusTimer = value; }
    
    public string CurrentLanguage { get => currentLanguage; set => currentLanguage = value; }

    public static DataBase Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        save.LG();
    }

}
