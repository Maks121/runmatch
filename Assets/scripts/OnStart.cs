using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class OnStart : MonoBehaviour

{
    public GameObject TileInst;
    [SerializeField] private LevelManager LM;
    [SerializeField] private GameMode GM;
    [SerializeField] private UIinGame UI;
    [SerializeField] private Board board;
    public GameObject PanelOnStart;
    public GameObject PanelTask;
    public GameObject panelLevelTask;
    public GameObject panelLevelTaskinGame;
    public GameObject prefabsImage;
    public GameObject prefabsTimer;
    
    public Image imageBack;



    
    // Start is called before the first frame update
    void Start()
    {
        LM = FindObjectOfType<LevelManager>();
        StartCoroutine(StartWait());
        EventStopGame.Instance.StopGameOnStart += EventStopGame_StopGameOnStart;
        EventStopGame.Instance.RunGameOnStart += EventStopGame_RunGame;
    }

    private void PanelActiveCountBotton(GameObject panel)
    {
  
            if (LM.curLvl.gameMode == Level.GameMode.Task)
            {

                for (int i = 0; i < LM.curLvl.spriteTask.Length; i++)
                {
                    if (LM.curLvl.spriteTask[i] != null)
                    {
                        Instantiate(prefabsImage, panel.transform);
                        panel.transform.GetChild(i).GetComponent<Image>().sprite = LM.curLvl.spriteTask[i];
                        panel.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = GM.countItem[i].ToString();
                    }

                }


            }
            else if (LM.curLvl.gameMode == Level.GameMode.Timer)
            {
                Instantiate(prefabsTimer, panel.transform);
                panel.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = LM.curLvl.timeLevel.ToString();
            }

    }
    private void EventStopGame_RunGame()
    {//��������� ������� ���� ���� �� ��a�������� ������ � �����
        for (int i = 0; i < TileInst.transform.childCount; i++)
        {
            TileInst.transform.GetChild(i).GetComponent<Collider2D>().enabled = true;
        }
    }
    private void EventStopGame_StopGameOnStart()
    {
        for (int i = 0; i < TileInst.transform.childCount; i++)
        {
            TileInst.transform.GetChild(i).GetComponent<Collider2D>().enabled = false;
        }
    }

    private IEnumerator StartWait()

    {
        //������ ������ � ���������
        PanelOnStart.SetActive(true);
        imageBack.DOFade(0.6f ,1f);
        EventStopGame.Instance.ActiveOnStart();
        yield return new WaitForSeconds(1f);
        
        PanelActiveCountBotton(panelLevelTask);
        PanelTask.transform.DOScale(Vector3.one, 1f);

        yield return new WaitForSeconds(2f);

        PanelTask.transform.DOScale(Vector3.zero, 1f);

        yield return new WaitForSeconds(1f);

        imageBack.DOFade(0f, 1f);
        
        yield return new WaitForSeconds(1f);
        PanelActiveCountBotton(panelLevelTaskinGame);
        PanelOnStart.SetActive(false);
        EventStopGame.Instance.EnActiveOnStart();
        if (LM.curLvl.gameMode == Level.GameMode.Timer)
        {
            StartCoroutine(GM.StartTimer(LM.curLvl.timeLevel));
        }
    }
}
