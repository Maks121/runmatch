using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;


public class Event : MonoBehaviour
{
    public delegate void EventDelegate();
    public event EventDelegate backPanel;
    public Image imageBack;
    private void ev()
    {
        backPanel?.Invoke();
    }

    public void ActiveBackpanel()
    {
        imageBack.DOFade(0.5f, 1f);
    }
    public void AnActiveBackpanel()
    {
        imageBack.DOFade(0f, 1f);
    }
}
