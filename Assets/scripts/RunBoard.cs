using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunBoard : MonoBehaviour
{

    public float speed;
    // Start is called before the first frame update
    void Awake()
    {
        EventStopGame.Instance.StopGameOnStart += Events_StopGame;
        EventStopGame.Instance.RunGameOnStart += Events_RunGame;
    }

    private void Events_RunGame()
    {
        speed = 0.21f;
    }

    private void Events_StopGame()
    {
        speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}
