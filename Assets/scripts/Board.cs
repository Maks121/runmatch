using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Runtime.Game.Crystal;
using Zenject;
using Random = UnityEngine.Random;


public class Board : MonoBehaviour
{
    private int xSize, ySize;

    private GameObject[,] tiles;
    private Vector2 offset;
    [SerializeField] LevelManager LM;

    public Crystal tile;
    public GameObject tileStone;
    public List<Sprite> characters;

    private CrystalFactory crystalFactory;

    [Inject]
    public void Construct(CrystalFactory crystalFactory)
    {
        this.crystalFactory = crystalFactory;
    }

    void Start()
    {
        LM = FindObjectOfType<LevelManager>();
        xSize = LM.curLvl.xSize;
        ySize = LM.curLvl.ySize;
        transform.position = new Vector3(transform.position.x, -((ySize + 3f) * 0.8f), 0);
        characters = new List<Sprite>(LM.curLvl.Characters);

        offset = tile.GetComponent<SpriteRenderer>().bounds.size;
        CreateBoard(offset.x, offset.y);
        StartCoroutine(CreateStoneTile());
    }

    public void InstBoard()
    {
        CreateBoard(offset.x, offset.y);
    }

    private void CreateBoard(float xOffset, float yOffset)
    {
        tiles = new GameObject[xSize, ySize];

        var startX = transform.position.x;
        var startY = transform.position.y;

        Sprite[] previousLeft = new Sprite[ySize]; // Add this line
        Sprite previousBelow = null; // Add this line

        for (var x = 0; x < xSize; x++)
        {
            for (var y = 0; y < ySize; y++)
            {
                var newTile = crystalFactory.Instantiate(tile,
                    new Vector3(startX + (xOffset * x), startY + (yOffset * y), 0),transform);
                newTile.GetComponent<TileControl>().levelDefence = 1;
                tiles[x, y] = newTile.gameObject;
                newTile.transform.parent = transform; // Add this line

                var possibleCharacters = new List<Sprite>();
                possibleCharacters.AddRange(characters);

                possibleCharacters.Remove(previousLeft[y]);
                possibleCharacters.Remove(previousBelow);

                var newSprite = possibleCharacters[Random.Range(0, possibleCharacters.Count)];
                newTile.GetComponent<SpriteRenderer>().sprite = newSprite;
                previousLeft[y] = newSprite;
                previousBelow = newSprite;
            }
        }
    }

    private IEnumerator CreateStoneTile()
    {
        yield return new WaitForSeconds(.5f);
        var GM = gameObject.transform.childCount;
        List<GameObject> tileS = new List<GameObject>();
        for (int i = 0; i < GM; i++)
        {
            tileS.Add(gameObject.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < LM.curLvl.CountStoneTile; i++)
        {
            var t = tileS[Random.Range(0, tileS.Count)];
            Transform transformTile = t.transform;
            GameObject gmTile = t.gameObject;
            if (gmTile.GetComponent<TileControl>().levelDefence == 1)
            {
                gmTile.GetComponent<TileControl>().levelDefence = 2;
                Instantiate(tileStone, transformTile);
            }
        }
    }
}