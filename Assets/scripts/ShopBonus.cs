using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShopBonus : MonoBehaviour
{

    [SerializeField] private Bag bag;
    [SerializeField] private ActivationBonuses bonus;
    public Image imageTime;
    public Image imageLightning;
    public Image imageDynamite;

    public Image[] imageBonus;
    public Text[] textBonus;
    public Text[] textPriceBonus;

    public Button[] buttonCountBonus;
    public Button[] buttonBonusInGame;

    public GameObject panelShop;




    private int bonusActive;

    [Header("��������� �������")]
    public int[] countGoldLightning;
    public int[] countGoldDynamite;
    public int[] countGoldTimer;
    private int[] countBonus;


    private void Start()
    {
        ChangeOnButtonInGame();
    }
    private void ChangeOnButtonInGame()
    {
        int[] a = new int[] { bag.BonusLightning, bag.BonusDynamite, bag.BonusTimer };
        
        for (int i = 0; i < a.Length; i++)
        {
            var v = buttonBonusInGame[i].transform.GetChild(2).gameObject;
            if (a[i] == 0) v.SetActive(true);
            else v.SetActive(false);
 

        }
       
    }
    public void OnButtonInGame(string buttonName)// ����� �� ������ ������ ������
    {
        switch (buttonName)
        {
            case "lightning":
                if (bag.BonusLightning == 0)
                {
                    OnButton(buttonName);
                }
                else
                {
                    bonus.activeLightning = true;
                    FadeInBonuses();
                    //����� ���������� ������
                }
                break;

            case "dynamite":
                if (bag.BonusDynamite == 0)
                {
                    OnButton(buttonName);
                }
                else
                {
                    bonus.activeDynamite = true;
                    FadeInBonuses();
                    //����� ���������� ������
                }
                break;

            case "timer":
                if (bag.BonusTimer == 0)
                {
                    OnButton(buttonName);
                }
                else
                {
                    bonus.ActiveTimer();
                    FadeInBonuses();
                    //����� ���������� ������
                }
                break;
        }
        ChangeOnButtonInGame();
       
    }
    private void FadeInBonuses()
    {
        //������� ���������� ��� ������ ������

    }
    private void OnButton(string buttonName)
    {
        EventStopGame.Instance.Active();
        EventStopGame.Instance.ActiveOnStart();
        switch (buttonName)
        {
            case "lightning":
                textBonus[0].text = "������";
                textBonus[1].text = "���������� ��� ����� ����� ���������� �����";
                ActivePanel(imageLightning);
                bonusActive = 0;
                countBonus = countGoldLightning;
                break;

            case "dynamite":
                textBonus[0].text = "�������";
                textBonus[1].text = "���������� ��������� ������";
                ActivePanel(imageDynamite);
                bonusActive = 1;
                countBonus = countGoldDynamite;
                break;

            case "timer":
                textBonus[0].text = "����";
                textBonus[1].text = "������������� ����� �� 5 ������";
                ActivePanel(imageTime);
                bonusActive = 2;
                countBonus = countGoldTimer;
                break;
        }
        panelShop.transform.DOScale(Vector3.one, 0.5f);
        InteractableButtonBonus();
        ChangeText();
        
    }
    private void ChangeText()
    {// ������ ���� �� �������
        for (int i = 0; i < textPriceBonus.Length; i++)
        {
            textPriceBonus[i].text = countBonus[i].ToString();
        }
    }
    private void ActivePanel(Image image)
    {// ������ ������� �� �������, � ����������� ����� �������
        for (int i = 0; i < imageBonus.Length; i++)
        {
            imageBonus[i].sprite = image.sprite;
        }
    }
    private void InteractableButtonBonus()
    {//��� ������ ����� �� ������� �����(� ������ ������ ������ �� ������������� ������)
        for (int i = 0; i < buttonCountBonus.Length; i++)
        {
            if (bag.Gold >= countBonus[i])
            {
                buttonCountBonus[i].interactable = true;
            }
            else buttonCountBonus[i].interactable = false;

        }
    }
    public void OnButtonBonus(int button)// ����� �� ������� �������
        //�������� ������, ���� �����
    {
        if (bag.Gold >= countBonus[button])
        {

            StartCoroutine(ChengedGold(countBonus[button]));
            StartCoroutine(ChengedBonus(button));

        }
    }
    public void ExitPanel()
    {
       
        panelShop.transform.DOScale(Vector3.zero, 0.5f);
        EventStopGame.Instance.EnActive();
        EventStopGame.Instance.EnActiveOnStart();
        bag.SaveInDataBase();
    }
    private  IEnumerator ChengedGold( int c)
    {
        //������� �� ����
        for (int i = 0; i < c; i += 10)
        {
            bag.Gold -= 10;
            yield return new WaitForSeconds(0.01f);
        }
 
    }
    private IEnumerator ChengedBonus( int a)
    {
        //������� �� �����
        int b = ((a + 1) * 3);//������� ��� ��������� �����
        for (int i = 0; i < b; i++)
        {
            switch (bonusActive)//����� ����� ������� ��� � ��������
            {
                case 0:
                    bag.BonusLightning++;
                    break;
                case 1:
                    bag.BonusDynamite++;
                    break;
                case 2:
                    bag.BonusTimer++;
                    break;
            }
            yield return new WaitForSeconds(.2f);
        }
        ChangeOnButtonInGame();
    }
}
