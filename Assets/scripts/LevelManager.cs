using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;



public class LevelManager : MonoBehaviour
{
   
    [SerializeField] private Save save;
    
    public GameObject onDestroy;
    public Level[] Levels;
    public Level curLvl;
    public List<object> ListLevels;
    public GameObject panelButton;
    public Sprite spritesButtons;
    public Sprite spritesStars;
    public GameObject backImage;
    public GameObject panelStar;

    public GameObject panelLevel;

    private bool activePanelTask = false; 




    public int currentIndex;

    // Start is called before the first frame update
    public void CountButton()
    {// ������ ������ ����� ������ ������
        RunLevel(currentIndex);

    }
    public void PanelActiveCountBotton(int index)
    {// ����� ������ ������ � �������� � ���������� ������ ������
        if (!activePanelTask)
        {
            currentIndex = index;
            if (DataBase.Instance.CountLevel == index)
            {
                RunLevel(currentIndex);
            }
            else
            {

                StartCoroutine(StartWait());
                panelLevel.transform.GetChild(1).GetComponent<Text>().text = (currentIndex + 1).ToString();
                if (DataBase.Instance.LvlStar.Length != 0)
                {
                        for (int a = 0; a < DataBase.Instance.LvlStar[index]; a++)
                        {
                            panelStar.transform.GetChild(a).GetComponent<Image>().sprite = spritesStars;
                        }
                }
                activePanelTask = true;

            }
        }
        else
        {
            StartCoroutine(StartWaitExit());
            
            activePanelTask = false;
        }
        DataBase.Instance.BonusDynamite += 100;
        DataBase.Instance.BonusLightning += 100;
        DataBase.Instance.BonusTimer += 100;
        save.SG();

    }
    private IEnumerator StartWait()
    {
        backImage.SetActive(true);
        Image image = backImage.GetComponent<Image>();
        image.DOFade(0.6f, 1f);
        yield return new WaitForSeconds(1f);
        panelLevel.transform.DOScale(Vector3.one, 0.5f);

    }
    private IEnumerator StartWaitExit()
    {
        panelLevel.transform.DOScale(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(1f);

        Image image = backImage.GetComponent<Image>();
        image.DOFade(0f, 1f);

        yield return new WaitForSeconds(1f);
        backImage.SetActive(false);
    }
        
        


    private void Start()
    { 
     save.LG();
     ListLevels = new List<object>(Levels);

        for (int i = 0; i < panelButton.transform.childCount; i++)
        {
            panelButton.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
            
            if (i <= DataBase.Instance.CountLevel)
            {
                panelButton.transform.GetChild(i).GetComponent<Button>().interactable = true;
                panelButton.transform.GetChild(i).GetComponent<Image>().sprite = spritesButtons;

                if (DataBase.Instance.LvlStar.Length != 0)
                {
                        
                        
                        for (int a = 0; a < DataBase.Instance.LvlStar[i]; a++)
                        {
                            panelButton.transform.GetChild(i).GetChild(1).GetChild(a).GetComponent<Image>().sprite = spritesStars;
                        }
                }
                else
                {
                    DataBase.Instance.LvlStar = new int[50];
                    DataBase.Instance.SoundOn = true;
                   
                    save.SG();
                }
            }
            else
            {
                panelButton.transform.GetChild(i).GetComponent<Button>().interactable = false;
            }

        }
        DontDestroyOnLoad(onDestroy);
    }
   

    private void RunLevel(int indexBttn)
    {
     
        //�������� ������ � ������� � ��������� �����
        curLvl = ListLevels[indexBttn] as Level;
    
        //DontDestroyOnLoad(onDestroy);
        SceneManager.LoadScene("GameLevel");
    }
}
[Serializable]
public class Level 
{
    public Sprite[] Characters;
    public int xSize, ySize;
    public int CountStoneTile;
    public enum GameMode
    {
        Timer,
        Task,
        
    }
    public GameMode gameMode;

    [Header("Task Setting")]
    public Sprite[] spriteTask;
    public int[] countItem;
    
    [Header("Timer Setting")]
    public float timeLevel;

    [Header("Star Create Time")]
    public float timeCreateStar;

}
   