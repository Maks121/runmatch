using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
     private bool active = true;
    public GameObject panelGameOver;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Tile"))
        {
            if (active)
            { 
                panelGameOver.SetActive(!panelGameOver.activeSelf);
                EventStopGame.Instance.Active();
                EventStopGame.Instance.ActiveOnStart();
                active = false;
            }
            //��� ������ �� ������!!
        }
    }

}
