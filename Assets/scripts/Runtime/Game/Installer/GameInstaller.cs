﻿using Runtime.Game.Crystal;
using Zenject;

namespace Runtime.Logo
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<CrystalFactory>().AsSingle();
        }
        
    }
}