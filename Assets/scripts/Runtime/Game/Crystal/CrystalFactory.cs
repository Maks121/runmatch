﻿using UnityEngine;

namespace Runtime.Game.Crystal
{
    public class CrystalFactory
    {
        public Crystal Instantiate(Crystal prefab,Vector3 position,Transform parent)
        {
           var crystal = Object.Instantiate(prefab, parent);
           crystal.transform.position = position;
           
           return crystal;
        }

        public void LoadAsset()
        {
            
        }
    }
}