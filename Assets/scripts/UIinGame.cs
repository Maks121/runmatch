using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class UIinGame : MonoBehaviour
{
    [SerializeField] private LevelManager LM;
    [SerializeField] private Save save;
    [SerializeField] private RunBoard runBoard;
    [SerializeField] private GameMode gameMode;

    public GameObject panelTimer;

    
    public GameObject panel;
    public Text[] textTask;


    public GameObject panelComplate;
    
    public GameObject panelGameOver;

    public GameObject ImageBack;

    public GameObject TileParent;
    [Header("Setting")]
    public GameObject buttonSetings;
    public GameObject panelSettingSlide;
    private bool activeSettings = false;

    public GameObject buttonPause;
    private Vector3 buttonPauseTransform;
    private bool activePause = false;

    public Text scoreText;
    // Start is called before the first frame update
  
    void Start()
    {
        LM = FindObjectOfType<LevelManager>();
        
        if (LM.curLvl.gameMode == Level.GameMode.Timer)
        {
            panelTimer.SetActive(!panelTimer.activeSelf);
        }
        buttonPauseTransform = buttonPause.transform.position;
    }

    
    // Update is called once per frame
    void Update()
    {
        scoreText.text ="Score: "  + DataBase.Instance.Score.ToString();

    }
    public void Pause()
    {
        if (!activePause)
        {
            Vector3 pos = new Vector3(buttonSetings.transform.position.x, buttonSetings.transform.position.y, buttonSetings.transform.position.z);
            buttonPause.transform.DOMove(pos, 1f);
            activePause = true;
            Settings();
        }
        else
        {
            buttonPause.transform.DOMove(buttonPauseTransform, 1f);
            activePause = false;
            Settings();
        }
        
    }
    public void Settings()
    {
        var obj = panelSettingSlide.GetComponent<RectTransform>();
        if (!activeSettings)
        {
            buttonSetings.transform.DORotate(new Vector3(0, 0, -180), .7f);
            obj.DOSizeDelta(new Vector2(100, 365), .7f);
            activeSettings = true;
            StartCoroutine(AnimationSettingButton());
            ImageBack.SetActive(true);
            Image image = ImageBack.GetComponent<Image>();
            image.DOFade(.6f, 0.5f);
            EventStopGame.Instance.Active();
            EventStopGame.Instance.ActiveOnStart();
        }
        else
        {
            if (activePause)
            {
                Pause();
            }
            buttonSetings.transform.DORotate(new Vector3(0, 0, -.1f), .7f);
            obj.DOSizeDelta(new Vector2(100, 88), .7f);
            ImageBack.SetActive(false);
            Image image = ImageBack.GetComponent<Image>();
            image.DOFade(0f, 0.5f);
            activeSettings = false;
            EventStopGame.Instance.EnActive();
            EventStopGame.Instance.EnActiveOnStart();
            
        }
        
    }
   private IEnumerator AnimationSettingButton()
    {
        yield return new WaitForSeconds(.7f);

        for (int i = 0; i < panelSettingSlide.transform.childCount; i++)
        {
            panelSettingSlide.transform.GetChild(i).GetComponent<Animation>().Play();
        }
    }

    public void NextLevel()
    {
        if (LM.currentIndex == DataBase.Instance.CountLevel)
        {
            DataBase.Instance.CountLevel++;
            save.SG();
        }
        SceneManager.LoadScene("Menu");
        Destroy(GameObject.FindGameObjectWithTag("OnDestroy"), .1f);
    }
    public void MainMenu()
    {
        save.SG();
        SceneManager.LoadScene("Menu");
        Destroy(GameObject.FindGameObjectWithTag("OnDestroy"), .1f);
    }
    public void NewGame()
    {
        save.SG();
        SceneManager.LoadScene("GameLevel");
    }


    private IEnumerator ActivePanel(GameObject panel)
    {
        //������� ������ � ���������
        EventStopGame.Instance.Active();
        EventStopGame.Instance.ActiveOnStart();
        ImageBack.SetActive(true);
        Image image = ImageBack.GetComponent<Image>();
        image.DOFade(0.6f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        panel.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(0.5f);
        

    }
    private IEnumerator ClosePanel(GameObject panel)
    {
        //������� ������ � ���������
        
        panel.transform.DOScale(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.5f);
       
        Image image = ImageBack.GetComponent<Image>();
        image.DOFade(0f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        ImageBack.SetActive(false);
        EventStopGame.Instance.EnActive();
        EventStopGame.Instance.EnActiveOnStart();

    }
}
