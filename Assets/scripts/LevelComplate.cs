using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelComplate : MonoBehaviour
{
    [SerializeField] private LevelManager LM;

    public GameObject prefabStar;
    public GameObject panelStar;
    public GameObject partical;


    public int countStar;
    public  float currentTime;
    private bool stopGame = false;
  
    private void Start()
    {
        LM = FindObjectOfType<LevelManager>();
        StartCoroutine(StartTimer(LM.curLvl.timeCreateStar));
        EventStopGame.Instance.StopGame += EventStopGame_StopGame;
        EventStopGame.Instance.RunGame += EventStopGame_RunGame;
    }

    private void EventStopGame_RunGame()
    {
        stopGame = false;
        StartCoroutine(StartTimer(currentTime));
    }

    private void EventStopGame_StopGame()
    {
        stopGame = true;
    }

    public IEnumerator AnimationStar()
    {
        List<GameObject> listStar = new List<GameObject>(); 
        int a = 0;
        while (a < countStar) 
        {
            Transform Gm = panelStar.transform.GetChild(a).transform;
            GameObject newGo = Instantiate(prefabStar, Gm);
            
            listStar.Add(newGo.gameObject);
            listStar[a].transform.DOScale(new Vector2(1.2f,1.2f), 0.8f);
            yield return new WaitForSeconds(0.8f);
            Instantiate(partical, Gm);
            listStar[a].transform.DOScale(Vector2.one, 0.2f);
            yield return new WaitForSeconds(0.2f);
            a++;
        }
        yield break;
    }
    private IEnumerator<WaitForSeconds> StartTimer(float time)
    {
        //������� ������ �� ���������� ������� ���� ����
        float timer = time;
        currentTime = timer;
        while (timer > 0)
        {
            if (stopGame)
            {
                currentTime = timer;

                break;
            }
            yield return new WaitForSeconds(1.0f);
            timer--;
            if (timer <= (currentTime * (1 / 3)))
            {
                countStar = 1;
            }
            else if (timer >= (currentTime * (1 / 3)) && timer <= (currentTime * (2 / 3)))
            {
                countStar = 2;
            }
            else if (timer >= (currentTime * (2 / 3)))
            {
                countStar = 3;
            }
        }
    }
}
